class ShortUrlsController < ApplicationController
  require 'csv'
  def new
    @url_datum = UrlDatum.new
  end

  def index
    @url_data = UrlDatum.all
  end

  def create
    @url_datum = UrlDatum.generate(url_params[:url])
    flash['notice'] = 'Created Short Url'
    redirect_to action: :show, url: @url_datum.mapped_url
  end

  def upload_urls
    @url_datum = UrlDatum.new
  end

  def show
    @url_datum = UrlDatum.get_url_datum(params[:url])
  end

  def original_url
    url_datum = UrlDatum.find_by(shortened_url: params[:url])
    redirect_to 'http://' + url_datum.mapped_url
  end

  def create_uploaded_urls
    transaction_successful = false
    ActiveRecord::Base.transaction do
      CSV.foreach(params[:short_url][:csv_file].path) do |row|
        UrlDatum.generate(row[0])
      end
      transaction_successful = true
    end
    if transaction_successful
      redirect_to :root, notice: 'Urls Successfully Created.'
    else
      redirect_to :root, alert: 'Upload a valid CSV file'
    end
  end

  private
  def url_params
    params.require(:url_datum).permit(:url)
  end
end
