class V1::ShortUrlController < V1::BaseController
  before_action :set_url_datum, only: [:show, :destroy]
  def create
    @url_datum = UrlDatum.generate(params[:url])
    render :show, status: 201
  end

  def show
    puts "------------#{@url_datum.inspect}"
  end

  def index
    @url_data = UrlDatum.all
  end

  def destroy
    @url_datum.destroy
    render json: {}, status: 204
  end

  private
  def set_url_datum
    mapped_url = UrlDatum.get_mapped_url params[:url]
    @url_datum = UrlDatum.find_by(mapped_url: mapped_url)
    @url_datum ||= UrlDatum.find_by(shortened_url: params[:url])
    if @url_datum.nil?
      render json: {message: 'Bad Request'}, status: 400
      return
    end
  end
end
