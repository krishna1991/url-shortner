class UrlDatum < ApplicationRecord
  serialize :original_urls, Array

  validates_presence_of :original_urls, :mapped_url
  validates_presence_of :shortened_url, on: :update
  validates_uniqueness_of :mapped_url, :shortened_url
  after_create :set_shortened_url

  CHAR_MAPPING = {}

  (('a'..'z').to_a + ('A'..'Z').to_a + (0..9).map(&:to_s)).each_with_index {|c, i| CHAR_MAPPING[i] = c}

  def self.generate(url)
    if url_datum = get_url_datum(url)
      url_datum.add_url url
      url_datum
    else
      url_datum = UrlDatum.new(original_urls: [url])
      url_datum.set_mapped_url
      url_datum.save!
      url_datum
    end
  end

  def self.get_url_datum(url)
    find_by(mapped_url: get_mapped_url(url))
  end

  def self.generate_shortened_url number
    number += 100000 # for making shortened_url atleast 3 characters
    digits = []
    while number > 0
      remainder = number % base_length
      digits.push CHAR_MAPPING[remainder]
      number /= base_length
    end
    digits.reverse.join
  end

  def self.get_mapped_url url
    mapped_url = url.dup
    mapped_url.gsub!(/\Ahttps?:\/\//, '')
    mapped_url.gsub!(/\Awww\./, '')
    mapped_url
  end

  def set_mapped_url
    # based on id
    self.mapped_url = self.class.get_mapped_url(original_urls.first)
  end

  def add_url url
    return false unless mapped_url == self.class.get_mapped_url(url)
    return true if original_urls.include?(url)
    update_attribute(:original_urls, original_urls.push(url))
  end

  def url
    original_urls[0]
  end

  private

  def self.base_length
    @base_length ||= CHAR_MAPPING.keys.length
  end

  def set_shortened_url
    self.update_attribute(:shortened_url, self.class.generate_shortened_url(self.id))
  end

end
