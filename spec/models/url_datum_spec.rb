require 'rails_helper'

RSpec.describe UrlDatum, type: :model do
  let(:same_urls) { %w(google.com www.google.com http://google.com http://www.google.com  https://google.com https://www.google.com) }
  let(:url2) { 'www.facebook.com' }

  context 'validations' do
    it { should validate_presence_of(:original_urls) }
    it { should validate_presence_of(:shortened_url).on(:update) }
    it { should validate_presence_of(:mapped_url) }
    it { should validate_uniqueness_of(:shortened_url) }
    it { should validate_uniqueness_of(:mapped_url) }
  end

  context 'generating mapping urls' do
    it 'should create same url' do
      expect(same_urls.map{|u| UrlDatum.get_mapped_url(u)}).to all(be == 'google.com')
    end

    it 'should create different urls' do
      mapped_url_1 = UrlDatum.get_mapped_url(same_urls[0])
      mapped_url_2 = UrlDatum.get_mapped_url(url2)
      expect(mapped_url_1).not_to eq(mapped_url_2)
    end
  end

  context 'url creations' do
    it "should create a UrlDatum Object" do
      url_datum = UrlDatum.generate(same_urls[0])
      expect(url_datum.persisted?).to be_truthy
    end

    it 'should generate same UrlDatum Object' do
      url_data = same_urls.map{|u| UrlDatum.generate(u)}
      expect(url_data).to all(be == url_data[0])
    end

    it 'should generate different UrlDatum objects' do
      url_datum_1 = UrlDatum.generate(same_urls[0])
      url_datum_2 = UrlDatum.generate(url2)
      expect(url_datum_1).not_to eq(url_datum_2)
    end
  end

  context 'encoding function' do
    it 'create a valid base 62 encoded' do
      sample_url = '89bA3'
      number = 55 + 26*(62) + 1*(62**2) + 61*(62**3) + 60*(62**4)
      expect(UrlDatum.generate_shortened_url(number-100000)).to eq sample_url
    end
  end

  context 'adding an Url' do
    let(:url_datum) { UrlDatum.generate(same_urls[0]) }
    it 'should add the new url' do
      url_obj = FactoryGirl.create(:url_datum)
      expect(url_obj.add_url('https://www.cricinfo.com')).to be_truthy
      expect(url_obj.original_urls.count).to be 3
    end

    it 'should not add the same url again' do
      url_datum.add_url same_urls[1]
      present_count = url_datum.original_urls.count
      url_datum.add_url same_urls[1]
      expect(url_datum.original_urls.count).to be present_count
    end

    it 'should not add an invalid url' do
      expect(url_datum.add_url(url2)).to be_falsy
    end
  end


end
