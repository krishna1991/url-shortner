require 'rails_helper'

describe V1::ShortUrlController do
  before :each do
    request.env["HTTP_ACCEPT"] = 'application/json'
  end
  describe 'POST create' do
    context 'valid url' do
      let(:valid_url) {'www.hotstar.com'}
      it 'creates a shortened url in database' do
        expect{
          post :create, params: { url: valid_url }
        }.to change(UrlDatum, :count).by(1)
      end
      it 'sends status 201' do
        post :create, params: { url: valid_url }
        expect(response.status).to eq(201)
      end
      it 'assigns a shortened url' do
        post :create, params: { url: valid_url }
        expect(assigns(:url_datum)).to eq(UrlDatum.generate(valid_url))
      end
    end
  end

  describe 'GET index' do
    let(:url) { 'www.hotstar.com' }
    it 'assigns urls data to url_data' do
      get :index
      url_datum = UrlDatum.generate(url)
      expect(assigns(:url_data)).to eq([url_datum])
    end
    it 'render index template' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'SHOW original urls from shortened url' do
    before do
      @shortened_url = UrlDatum.generate('www.hotstar.com').shortened_url
      get :show, params: { url: @shortened_url}
    end
    it 'assigns url datum' do
      expect(assigns(:url_datum)).to eq(UrlDatum.generate('www.hotstar.com'))
    end
    it 'renders show template' do
      expect(response).to render_template(:show)
    end
  end

  describe 'DELETE shortened url' do
    before do
      @shortened_url = UrlDatum.generate('www.hotstar.com').shortened_url
    end
    context 'valid shortened url' do
      it 'deletes the shortened url' do
        expect{
          delete :destroy, params: { url: @shortened_url}
        }.to change(UrlDatum, :count).by(-1)
      end
      it 'sends a status 204' do
        delete :destroy, params: { url: @shortened_url}
        expect(response.status).to eq(204)
      end
    end
  end
end
