FactoryGirl.define do
  factory :url_datum do
    original_urls ['http://cricinfo.com', 'www.cricinfo.com']
    mapped_url "cricinfo.com"
    shortened_url 1
  end
end
