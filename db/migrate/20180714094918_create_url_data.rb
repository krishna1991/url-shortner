class CreateUrlData < ActiveRecord::Migration[5.1]
  def change
    create_table :url_data do |t|
      t.string :original_urls, limit: 4.kilobytes
      t.string :mapped_url
      t.string :shortened_url

      t.timestamps
    end
    add_index :url_data, :mapped_url
    add_index :url_data, :shortened_url
  end
end
