Rails.application.routes.draw do

  root to: 'short_urls#index'
  resources :short_urls, only: [:new, :index, :create], param: :url do
    collection do
      get 'upload_urls'
      get 'show'
      post 'create_uploaded_urls'
    end
  end

  namespace 'v1' do
    resources :short_url, param: :url, defaults: {format: :json}, only: [:index, :create]
    get 'shortened_url' => 'short_url#show'
    delete 'shortened_url' => 'short_url#destroy'
  end

  get '*url', to: 'short_urls#original_url'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
